\section{SYSTEM DESIGN}
\label{sec:sys_des}

In this section, we present our design of the EchoSpot system, aiming to leverage only one speaker and one  microphone   available on off-the-shelf (COTS) device as the sensing tool to implement precise human localization. 
We program the audio device to control its built-in speaker to emit inaudible signals at 18kHz$\sim$23kHz and use the co-located microphone to receive the reflected signals from objects for analysis. 
For localization, a series of solutions need to be developed to identify the time-of-flight (ToF) for the signals of interests.
In this context, a number of technical challenges are encountered to be addressed, briefly summarized as follows. 




\begin{itemize}		
	
	\item The signal modulation is important to determine the resolution of capturing a person's subtle movement, which further limits localization precision. 
	While we only rely on the COTS devices, the suitable method to modulate the emitting signals that can be implemented on while having the potential of achieving high resolutions for serving our purpose has to be determined, which is important while challenging. 
	
	
	
	\item The received signals are weak and mix the superposition of signals reflected from environment objects and multipath.
	How to locate the signals of interest (i.e. direct reflection) from the human body is a challenging problem, requiring us to develop a collection of techniques for effectively mitigating the interference from environment objects and multipath reflection.
	

	
	\item The target ToF can only help identify the distance of a human to the device rather than the position.
	In general, multiple pairs of speakers and microphones are required to pinpoint the human position by finding the intersection points of their respective ellipse curves after getting the distances. 
	However, EchoSpot is expected to only employ one   speaker and one microphone, which is more general in home devices.
	It remains challenging to determine the position information in the two-dimensional space. 
	
	
\end{itemize} 





\subsection{System Overview}

Our design of EchoSpot  consists of four components: {\em Signal Generation}, {\em ToF Profile Generation}, {\em Localization}, and {\em Location Correction}, as shown in Figure~\ref{systemdesign}.
In the {\em Signal Generation} module, EchoSpot programs the speaker to emit inaudible acoustic signals at 18kHz$\sim$23kHz. 
Specifically, it modulates the FMCW signal and periodically sends FMCW chirps while using the microphone to record the reflected FMCW signals.
The sampling frequency of the speaker is set to 48kHz. According to the  Nyquist Sampling Theorem~\cite{shannon1949communication}, the reflected signal can be entirely reconstructed from the recorded signals. 


The signal is then sent to the {\em ToF Profile Generation} module to generate the ToF profile for further analysis.
This module contains three components, i.e., {\em Cross Correlation}, {\em Starting Time Error Cancellation}, and {\em Interference Cancellation}.
Particularly, in the first component, the cross-correlation is applied on the received signal and the transmitted signal, creating the raw TOF profiles.
We next identify the starting point of the profile and eliminate the starting time error caused by device imperfection.
As the TOF profiles contain peaks of static reflections, we cancel these peaks in the {\em Interference Cancellation} module, with the direct reflection remained. 



In the {\em Localization} module, we build a location model that utilizes the distances of direct reflection and of wall reflection to calculate the location.
We pick up the peaks of the human reflection and wall reflection from the residual TOF profile after the interference cancellation aforementioned,
and roughly estimate the distance to be fed into the location model. 
Finally, in the {\em Location Correction} module, we apply Kalman filter to further eliminate  the potential measurement errors caused by multipath effect,  system defects, etc., to improve the accuracy of location estimation.


\subsection{Signal Generation}

\begin{figure}
	\includegraphics[width=4.5in]{Fig/system.pdf}
	\caption{The workflow of EchoSpot.}
	\label{systemdesign}
\end{figure}


In {\em Signal Generation module}, EchoSpot controls  the speaker to generate the FMCW (Frequency Modulated Continuous Wave) \cite{sorrentino2012accurate, brugger2010fmcw} signals for sensing.
In particular, it periodically transmits chirps and within the duration of a chirp, the operating frequency keeps changing from $f_{\min}$ to $f_{\max}$.
So, for each chirp with the duration of time $T$, the frequency of signals can be expressed as 
\begin{equation}f(t)=f_{\min}+\frac{B t}{T},\end{equation}
where $B$ is the signal bandwidth, defined as $B=f_{\max}-f_{\min}$.
The phase of the transmitted FMCW signal can be expressed as the integration of $f(t)$ over time, i.e., 
\begin{equation}
\lambda(t)=2\pi\int_{0}^{t} f\left(t\right) d t=2\pi(f_{\min} t+B \frac{t^{2}}{2 T}). \label{eq2} 
\end{equation}
Then,  the FMCW signal could be expressed as $cos(\lambda(t))$.


After the speaker transmits the FMCW signal $cos(\lambda(t))$, the microphone will receive a reflected signal $\cos (\lambda(t-\tau))$, which can be considered as a time shifted version of the transmitted one with a delay of $\tau$.
Here,  $\tau$ is called as the time-of-flight (ToF), which is the time from when the signal is generated at the speaker to when the  signal reflected from the object is received at the microphone. 
In FMCW signal, according to the characteristic of frequency change, $\tau$ can  also be measured as follows:
\begin{equation}
\tau=\frac{\Delta f}{\left(\frac{\delta(f)}{\delta(t)}\right)} \label{eq_d}\;, 
\end{equation}
where $\Delta f$ represents the frequency shift and  $\delta f / \delta t$ denotes the frequency shift per unit of time.
Here, $\delta f / \delta t=B/T$ in each time period.  
The distance $R$ for the object that causes the direct reflection can then be calculated by
\begin{equation} 
R=\frac{c\tau}{2}=\frac{c \Delta f}{2\left(\frac{\delta(f)}{\delta(t)}\right)},
\end{equation}
where $c$ is the speed of sound. 

Since the duration of each FMCW chirp is $T$, the minimum frequency resolution of the FFT is $1/T$. 
The range resolution $\delta R$, which shows the ability of  FMCW signal to capture the minimum movement, is determined by the frequency resolution.
Then, we have
\begin{equation}
\delta R=\frac{c/T}{2\left(\frac{\delta(f)}{\delta(t)}\right)} = \frac{c}{2 B}. 
\label{basic} 
\end{equation}
Obviously, the range resolution is determined by the bandwidth $B$.

Considering that the commercial microphones can only record the signals below the 24kHz and the majority of background noises (such as human conversation, music, FM radio wave, etc.) have frequencies up to 14kHz, EchoSpot assigns the frequency of chirp  ranging from $f_{\min}$=18kHz to $f_{\max}=$23kHz, with the bandwidth of 5kHz.
As such, the range resolution can reach $3.4cm$ according to  Eqn.~(\ref{basic}), giving the sound speed $c$ of $340m/s$. 


Typically, the longer the duration time $T$ (chirp length),  the more the overlapped parts among reflected signals, which brings difficulty in differentiating them. 
However, the long duration would help the system to find the big reflector in the environment. 
On the other hand, \cite{schroeder2010x} has shown that a shorter duration of FMCW can lead to higher tracking accuracy since it will result in a smaller Doppler shift caused by the movement.
But considering the limits of home devices, if the duration time is too short, the sound energy becomes too weak so that the reflected signals may not be detected due to the low SNR.
Hence, choosing a duration time is very important to help identify the echo reflected from the body. 
We have conducted extensive experiments in different environments, aiming to identify an appropriate chirp duration $T$, which works in the home environment while minimizing the overlapping among reflected signals.
We found that if the duration is less than $0.02s$, less overlapping is observed; if it is longer than $0.005s$, the target signal is stronger to be detected. 
As such, we set the duration of $T$ as $0.01s$, resulting in strong enough signal for detecting the target while having less overlapping from the reflections. 



For the time interval between adjacent chirps, a short one would result in a high resolution of localization. 
However, the received signals may be severely affected by multipath effect from the previous chirp. 
On the other hand, considering a house environment, the maximum distance from a person to the device is around 7 meters.
Theoretically, the time interval should be larger than   $\frac{14 \mathrm{m}}{340 \mathrm{m} / \mathrm{s}} \approx 41.2 \mathrm{ms}$, for sufficiently receiving the reflection signals from the body. 
In EchoSpot, we set the time interval to be $200ms$ to ensure that it is sufficient to receive the reflection signals while the multipath effect from one chirp will not impact the next chirp. 

\begin{figure}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/windowed.pdf}		
		\caption{Autcorrelation of the windowed signal, with less sidelobes.}
		\label{windowed}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/unwindowed.pdf}		
		\caption{Autcorrelation of the unwindowed signal, with many sidelobes.}
		\label{unwindowed}
	\end{minipage}
	
	
\end{figure}

\noindent{\bf \em FMCW signal windowing.} \ 
Inspired by \cite{graham2015software}, we then apply the Hanning window to reshape the FMCW signal envelopes to increase the signal-to-noise ratio (SNR) by improving the peaks to the sidelobe ratio.
The windowed signal would have fewer sidelobes comparing to the raw signal without applying the window function.
We conduct an experiment to validate this point.
Figure~2 and Figure~3 exhibit the autocorrelation of the windowed signals and the raw signals, respectively.
It is obvious that the autocorrelation from the windowed signal in Figure~2 becomes stronger and has fewer sidelobes, resulting in a higher SNR.
This phenomenon would be similar when we perform the cross-correlation between the windowed signal and the reflected signal.
Fewer sidelobes would result in a higher SNR, thus improving the accuracy.



To conclude, EchoSpot will control the speaker to periodically send FMCW wave at the  frequency of 18kHz$\sim$23kHz with the time duration of $40ms$ for each chirp, the bandwidth of  5kHz, and the time interval of $200ms$ between two chirps.


\subsection{TOF Profile Generation}




\label{TOF Profile Generation}
After receiving the reflected signals, EchoSpot will measure the time of flight (ToF) of the respective signals, which will be further used to calculate the distances. 
Since FMCW signals range from 18kHz to 23kHz, we use a band-pass filter to extract the useful signals.
We will first generate the time-of-flight (ToF) profile and then develop a series of solutions to remove the potential measurement errors from the system starting time error and environmental interference. 







\smallskip
\noindent{\bf \em ToF Profile.} \ 
 We pick up a series of $N$ points from the transmitted signals and the received signals.
Denote $v_{t x}(m)$ and $v_{r x}(m)$ as the transmitted  and received signals, respectively, at a point $m$.
The normalized cross-correlation is then applied to measure the similarity of  transmitted signals and its $n$-sample shifted version of received ones, expressed as follows:
\begin{equation}
P(n)=\frac{\frac{1}{N}\sum_{m=0}^{N}[v_{r x}(m)-\bar{v}_{r x}][v_{t x}(m-n)-\bar{v}_{t x}]}{\left\{\sum_{m=0}^{N}[v_{r x}(m)-\bar{v}_{r x}]^{2} \sum_{m=0}^{N}[v_{t x}(m-n)-\bar{v}_{t x}]^{2}\right\}^{0.5}}\; , 
\label{correlation}
\end{equation}
where $\bar{v}_{t x} $ and $\bar{v}_{r x}$ indicate the  average values of the transmitted and received signals, respectively, over the $N$ points. 
By picking up the corresponding lag  of the peak on $P(n)$, we can transform it into the ToF value, so as to further calculate the distance value of the respective reflection object, i.e.,  
\begin{equation}
\tau=\frac{n}{F_{s}}, \label{delay}
\end{equation}
where $F_{s}$ is the sampling frequency, which is set as 48kHz in EchoSpot.

Figure~\ref{example} shows an example of the $P(n)$.
After getting the $P(n)$, we apply Hilbert Transform~\cite{hahn1996hilbert} to calculate the envelope $E(n)$ of  $P(n)$, as shown in Figure~\ref{example5}.
 where the $x$-axis represents the  TOF, and the $y$-axis represents the cross-correlation value which measures the similarity of the transmitted and the received signals.
In the remaining of this paper,  $E(n)$ will be called as the ToF profile.
Since we emit the FMCW signal   every 200ms, each ToF profile $E(n)$ will be generated for this period. 

From this step, we can roughly calculate the ToF profile, expressed by $E(n)$.
However, the unsychronization of the speaker and microphone  along with the environmental interference will significantly affect the measurement of ToF, making the ToF profile inaccurate.



\begin{figure*}%[!htb]
	\centering

	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2in]{Fig/sample2.pdf}
		\caption{TOF profile after eliminating the time error.}
		\label{example2}
	\end{minipage}
		\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/sample3.pdf}
		\caption{Denoised TOF profile, where strong peaks represents the  human reflection. Some other multipath reflections from  the human also remain.}
		\label{pure}
	\end{minipage}

\end{figure*}

\smallskip
\noindent{\bf \em Starting time error.} \ 
Since the speaker and the microphone are co-located, the direct transmission time can be ignored.
However, due to the unsynchronization between  speaker and  microphone (i.e., device imperfection), there is a certain delay for the  signals to be received by the microphone.
This delay is called the start time error. 
Since the direct transmission signal is directly received without reflection, it is stronger than all other reflected signals, allowing us to identify it by selecting the lag  with the largest peak value.
We  denote this  point as the start time point, whose corresponding time of flight indicates the start time error.
This point has to be correctly identified so as to  accurately perform the calculations in  remaining steps.
As shown in  Figure~\ref{example5}, the start time error is $0.107s$.
By  removing the peak value before the start time point in the Figure~\ref{example5}, we get the TOF profile without the start time error, as shown in Figure~\ref{example2}. 
In Figure~\ref{example2} the lag values directly correspond to the time of flight of the reflection signals, so we could use it to calculate the time of flight.







\noindent{\bf \em Interference Cancellation}
The environment object will also cause reflection, which thus generate a set of peaks in ToF profile, misleading our selection of these reflected signals from the human body. 
In Figure~\ref{example2}, we observe two main peaks, however, not all of them are from body reflection.
We will show how to remove the peaks that do not correspond to the body reflections so as to mitigate the interference from the environment reflection. 

Since the positions of objects in a room are fixed, their respective ToF files are relatively same within a certain time. 
Hence, EchoSpot records 10 ToF profiles in the static environment without human movement and calculates the averaged correlation value for each corresponding lag within these 10 ToF profiles.
We use a vector $\bar{\bf p}$ to indicate a series of averaged values corresponding to all lag points.
By subtracting $\bar{\bf p}$ from the peak values of ToF profile $E(n)$, we can eliminate those peaks corresponding to the environmental interference, resulting in a denoised ToF profile (Figure~\ref{pure}) with a vector of peak values denoted as ${\bf E}_{d}$.
Comparing to Figure~\ref{example2}, we could see the second peak is totally removed, with only one peak remaining which is the body reflection peak.



In the real-world deployment, it is not necessary to let the speaker keep sending the higher power FMCW signals for sensing. 
Considering that the environment state typically is static,  EchoSpot can periodically transmit high power FMCW signal for capturing the environment ToF in each 30 minutes for updating.
Meanwhile, EchoSpot can  periodically (say $10s$) transmit the FMCW signals at a low rate and low signal power (50\% of its working power) to sense the environment for detecting the appearance of  a person.
Once a person is detected from the low power signals, it will be immediately triggered to transmit the higher power signal at a higher rate to start working.
Then, EchoSpot can perform the subtraction of the environments from the current ToF profiles, which can result in the  pure ToF profiles used for  localization.
When the person leaves the room, the reflection signals will become weaker,  so EchoSpot will resume to the low power state.



\begin{figure*}%[!htb]
	\centering
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2in]{Fig/sample.pdf}
		\caption{Example of cross correlation. 0.10763 and 0.20905 indicate the start time error and its corresponding peak value.}
		\label{example}
	\end{minipage}
	\hspace{1.0em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2in]{Fig/sample5.pdf}
		\caption{Example of TOF profile, where the peaks refer to the reflections in the environment.}
		\label{example5}
	\end{minipage}	
\end{figure*}


\subsection{Localization}




\label{sec:localization}
After preprocessing in Section~\ref{TOF Profile Generation}, we next show how to choose the correct peaks on the residual ToF profile for calculating the distances, which can be eventually used for localization. 
Since our solution will rely on only one  speaker and one  microphone, it is not sufficient to use only the ToF from a human for localization.
Here, the ToF from wall reflection will assist us to spot a person's location. 
In what follows, we will first show  how to spot the location of a human with the assistance of wall and then illustrate how to  choose the ToFs corresponding to the human reflection and the wall reflection. 


\smallskip
\noindent{\bf \em Localization via One Speaker and One Microphone.} \ 
We will show how to model the location of a person when just using one speaker and microphone.
We assume the distance between  the wall and the device are known, which can be obtained through the following way: letting the speaker periodically send acoustic signals and the microphone receive the direct reflection, then we measure the time-of-flight from the reflected signal for calculating such a distance.
As shown  in Figure~\ref{wall}, the distance between the wall and the device is known, denoted as $w$.
$R$ and $S$ represent the person and the device, respectively.
We draw a point $S_{v}$ in the opposite side of the wall with a distance of $w$. 
In this figure, $(x, y)$ represents the relative distance of the person to the microphone, which we aim to determine.
The position of the speaker is assumed to be known, then we could model the position of $(x, y)$ as follows:
\begin{equation}\left\{\begin{array}{l}
x^{2}+y^{2}=d^{2} \;,\\
d+d_{v}=D \,,\\
(2 w-x)^{2}+y^{2}=d_{v}^{2} \;,
\end{array}\right.\end{equation}
where $d$ is the distance between the device and the person, $d_{v}$ is the distance from the person to $S_{v}$. 
$D$ represents the distance  from the device $S$ to the target $R$ and  then to $S_{v}$. 
This formula can be solved with solution of:
\begin{equation}\begin{array}{l}
x=\frac{-D^{2}+2 D d+4 w^{2}}{4 w} \;,\\
y=\sqrt{d^{2}-x^{2}} \;.
\end{array} \label{solution} \end{equation}
Hence,  $x$ and $ y$ can be seen as the functions of  $D$ and $d$.
Notably, $d$ can be calculated by the ToF corresponding to the body reflection and $D$ can be calculated by that corresponding to the wall reflection through the human body. 






\smallskip
\noindent{\bf \em Identification of ToFs for Body Reflection.} \ 
Through interference cancellation in Section~\ref{TOF Profile Generation},  the interference of  reflected signals from the static object  has been eliminated.
As shown in figure \ref{pure}, we could see a strong peak that corresponds to the human reflection.
Hence, we can directly pick up the strongest peak ${\bf E}_d$  from figure \ref{pure}, and treat the associated signal as being from the body reflection. 
The corresponding lag value, assuming $l_1$,  can be used to calculate the time delay $\tau_1$ and the distance $d$, i.e., 
\begin{equation}\begin{array}{l}
\tau_{1}=\frac{l_{1}}{F_{s}} , \  
d=c \frac{\tau_{1}}{2}
\end{array}\end{equation}
However, the residual multipath reflection will travel a longer distance and have a smaller peak value on ToF profile.
From the residual ToF profile on figure \ref{pure}, it is hard to directly pick up the peak from the wall reflection.

\begin{figure}	
\begin{minipage}{0.30\textwidth}
	\centering
	\includegraphics[width=2.0in]{Fig/wall.pdf}
	\caption{Location model, R indicate the people and S indicate the speaker.}
	\label{wall}
\end{minipage}
\hspace{1em}
\begin{minipage}{0.30\textwidth}
	\centering
	\includegraphics[width=2.0in]{Fig/reflection.pdf}
	\caption{Different reflection path when the signal reflect from the body, where d is the shortest path, b is the longest path, and h is the height of speaker.}
	\label{reflection}	
\end{minipage}
\hspace{1em}
\begin{minipage}{0.30\textwidth}
	\centering
	\includegraphics[width=2.0in]{Fig/sample4.pdf}
	\caption{Reflection from body and wall, the second peak is the reflection from the wall.}
	\label{sample4}
\end{minipage}

\end{figure}

\smallskip
\noindent{\bf \em Identification of ToFs for Wall Reflection.} \ 
We have two observations that could help us pick up the exact location of the wall reflection.
The first observation is that  the wall reflection signals are strong, which may be due to the large size of the wall, where different multipath signals aggregate together on the wall, resulting in a strong reflection.
That means the signal reflected from the body then to the wall is detectable.
The second observation is there would be a similarity between the wall reflection and body reflection.
When we apply the cross-correlation, we observe a relatively small peak on the TOF profile at the wall reflection distance.
The small peak means the wall reflection is similar to the original signal.
Also, the strong peak on the body reflection means the body reflection is similar to the original signal.
Since both the wall reflection and body reflection are  similar to the original  FMCW signal, this further implies there would be a similarity between the wall reflection and body reflection.




Based on the two observations, we propose a new solution, which can help us identify the reflection distance from the wall passed through the human body. 
We consider the  human body as a virtual sender and  the reflected signals from human body to the wall as the virtual sending signals.
Due to the similarity between the wall reflection and body reflection signal, we could apply the cross correlation to identify the wall reflection signal.



Figure~\ref{reflection} shows how the signal is reflected from the body. Typically there are several paths.
In the figure, $d$ is the shortest reflection path from the body to  microphone in the horizon direction. $b$ is the longest reflection path which is from feet to the microphone.
The body reflection signal length can be calculated as  {\em original signal length + (TOF of the longest path - TOF of the shortest path)}.
Since the body reflection signals are too long, which shall contain the reflection from other objects in the environment, we cannot directly take them as the virtual signals.
Instead, we only take  signals at the time interval between the longest  and short reflection as our  virtual signals, to ensure that the majority  are reflected from the body.




As shown in Figure~\ref{reflection}, the start time is determined by the shortest signal path, i.e., the distance from the microphone to the human $d$, which has been calculated in the last step.
The possible longest path can be considered as the reflection from the feet, with the distance denoted as $b=\sqrt{{d}^{2}+h^{2}}$. 
If we get the height of the speaker $h$, we could get the length of the longest path.
Then, the delay of this reflection comparing to the shortest path is $\hat t=2(b-d)/c$.
We set the duration to be around  $\hat t$ for the  virtual sending signals.




We take the FWCM signal with the duration of $\hat t$ and apply the cross-correlation on the received signals at the microphone via Eqn.~(\ref{correlation}) to get a new ToF profile, as shown in Figure~\ref{sample4}. 
Two sets of strong peaks  appear on such TOF profile.
%We observe that there are two sets of strong peaks in this ToF profile .
The first peak cluster can be considered as the direct transmission from the virtual sender (i.e., the reflection from the human body), while the second cluster can be considered as the nearby wall reflection. 
We apply the polynomial fit on the peak to estimate the body and wall reflection location.
As the figure shows, the orange curve is the polynomial fit curve.
We take the second strongest peak on polynomial fit curve as the wall reflection and assume its lag value as $l_2$.
Then, the time delay and distance (i.e., $D$)  from the body-wall-device can be calculated as follows:
\begin{equation}\begin{array}{l}
\tau_{2}=\frac{l_{2}}{F_{s}},
D=c\tau_{2}
\end{array}\end{equation}
After having the values of $d$ and $D$, we can apply them to Eqn.~(\ref{solution})  and get the position $(x, y)$ for the person $R$.

Notably, this solution relies on the location of wall. 
However, in practice, there may be two walls in the opposite position of the person.
Our solution still works as follows.
Assume the distances from the device to the two walls are $w_1$ and $w_2$, respectively.
Through the correlation, there will be three strongest peaks in the new ToF profile.
While the first one is still the direct transmission from the virtual sender to the device, for the following two, it is necessary to decide their correspondences to $w_1$ and $w_2$.
We can assume the first one and the second one are corresponding to $w_1$ and $w_2$, respectively, 
and use the aforementioned method to calculate two respective positions.
If the two calculated positions coincide, then we locate the person; otherwise, the first one and the second one correspond to $w_2$ and $w_1$, respectively.



\subsection{Correcting the Location}
Ideally, from aforementioned steps, we should have spotted the location $(x, y)$ of a person.
However, there may be  some potential errors due to the  wrong selection of peak values from the residual multipath reflection.
Considering the characteristic of a moving person, a series of his relative positions at different time points can be traced to further correct our calculation. 
Here, we leverage the Kalman filter~\cite{welch1995introduction} by taking into account  two consecutive measurements  for  correcting the current location. 
In Kalman filter, we consider two consecutive measurements and use the previous calculated location to predict the one at the next time point, which can be modeled as follows: 
\begin{equation}
\boldsymbol{\hat{x}}_{t}=\boldsymbol{F} \boldsymbol{\hat{x}}_{t-1}+\boldsymbol{B} a_{t},
\end{equation}
where $\boldsymbol{\hat{x}}_{t}=\left[\hat{p}_{t}, \hat{v}_{t}\right]^{T}$ and  $\hat{p}_{t}$ represents the predicted $(x, y)$ and $\hat{v}_{t}$ indicates the predicated velocity at time $t$.
$\hat{x}_{t-1}$ represents the prediction at the time point $t-1$.
%$\hat{x}_{t-1}$ is the prediction of the distance and velocity at time $t-1$.
${a}_{t}$ is the acceleration speed of the moving person. 
We assume the motion of a human contains both the acceleration and deceleration phases, so acceleration shall follow a Gaussian distribution with the mean of $0$.
\cite{lakoba2005modifications}  suggests the maximum acceleration speed of a walking person is 0.2g to 0.3g.
As the human would have a relatively low speed in indoor environment, we choose 0.6 as the variance $\sigma_{a}$ roughly.
Then the ${a}_{t}$ could be represented as $a_{t} \sim \mathcal{N}\left(0, \sigma_{a}^{2}\right)$.
 $\boldsymbol{F}$ is the transmission matrix and $\boldsymbol{B}$ is the control matrix, which can be expressed as:
\begin{equation}\boldsymbol{F}=\left[\begin{array}{cc}
1 & \Delta t \\
0 & 1
\end{array}\right], \boldsymbol{B}=\left[\begin{array}{c}
\frac{\Delta t^{2}}{2} \\
\Delta t
\end{array}\right],\end{equation}
where $\Delta t$ is the  elapsed time between two consecutive measurements.
Denoting  $\boldsymbol{P}_{t}$  as the  prediction covariance matrix at time $t$ and $\boldsymbol{Q}$ as the covariance matrix of the acceleration ${a}_{t}$, we have:
\begin{equation}
\boldsymbol{P}_{t}=\boldsymbol{F P}_{t-1} \boldsymbol{F^{T}}+\boldsymbol{Q}.
\end{equation}
Notably, $\boldsymbol{P}_{t}$ is determined by $a_{t}$.
According to Kalman filter, we have  
\begin{equation}\begin{array}{l}
\boldsymbol{z_{t}}=\boldsymbol{H {x}}_{t}+\boldsymbol{s}, \\
\boldsymbol{R}_{t}=\boldsymbol{H P}_{t} \boldsymbol{H^{T}},
\end{array}\end{equation}
where $\boldsymbol{z_{t}}= (x,y)$, calculated in Section~\ref{sec:localization}, and  $\boldsymbol{H}$ is defined as $\boldsymbol{H}=\left(\begin{array}{ll}1 & 0\end{array}\right)$, as we do not aim to calculate the velocity.
$\boldsymbol{{x}}_{t}$ is defined as the actual location which is the hidden state.
$\boldsymbol{s}$ is the observation noise of the system that could not be directly measured. 
$\boldsymbol{R}_{t}$ is the covariance matrix  corresponding to $(x, y)$.

Since $\boldsymbol{z_{t}}$ is known, we could update the estimation by
\begin{equation}
\boldsymbol{\hat{x}}_{t}^{\prime}=\boldsymbol{\hat{x}}_{t}+\boldsymbol{K}^{\prime}\left(\boldsymbol{z}_{t}-\boldsymbol{H \hat{x}}_{t}\right),
\end{equation}
where  $\boldsymbol{\hat{{x}}_{t}^{\prime}}$ is denoted as the updated estimation of the $x,y$.
$\boldsymbol{K^{\prime}}$ can be expressed as $\boldsymbol{K^{\prime}}=\boldsymbol{P}_{t} \boldsymbol{H^{T}}\left(\boldsymbol{H P}_{t} \boldsymbol{H^{T}}+\boldsymbol{R}_{t}\right)^{-1}$.
Through this way, we can get a better estimation of the current location.
This process can be continued so that we can have a series of new estimation of a person location at different time. 

