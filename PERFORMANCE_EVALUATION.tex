\section{PERFORMANCE EVALUATION}
\label{sec:performance}
In this section, we implement the EchoSpot system and deploy it in the real home environment for experiments.
A set of experiments has been conducted and analyzed to show the performance of the EchoSpot in terms of localization accuracy for  both static and moving person at different positions.


\subsection{Experiments}

We use one pair of  commodity speaker (Edifier R1280DB) and microphone (SAMSON MeteorMic, 16 bit, 48 KHz), and bind them together to work as our experimental devices. 
The output power of the speaker is set to around 80\% of the speaker's maximum power.
We measure the sound pressure level at  1 meter from the speaker, which is 45dB.
The speaker is programmed as the signal transceiver to transmit FMCW signals with the carrier frequency sweeping from 18kHz to 23kHz.
The microphone works as the signal receiver to record the reflected signals at a 48kHz sampling rate.
It is connected to a laptop and uploads the reflected signals to this laptop for processing.
Our localization algorithm is implemented on this laptop with Matlab. 


For performance validation, we mark some reference points and draw one trajectory with known positions on the floor to serve as the ground truth. 
They will be used to measure the performance of EchoSpot in terms of spotting the static locations and continuous locations, respectively. 
For static localization, we ask the target person to stand on each reference point for $10s$.
For continuous localization, the target person will be asked to walk along this predefined trajectory.
We consider the following performance measurement metrics:
\begin{itemize}

	\item \textbf{Localization Error} is the distance between the measured position and the ground truth reference point.


	\item \textbf{Trajectory Error} is the vertical distance between the walking trajectory and ground truth trajectory.
\end{itemize}

\subsection{Performance of Localization}


\begin{figure*}%[!htb]
	\centering
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/Distance_err.pdf}

		\caption{Averaged localization errors at $1m, 2m, 3m, 4m$ and $5m$.}
		\label{Distance}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/_rect.pdf}

		\caption{Tracking the moving trajectory of a person, where the red lines indicate the predefined rectangle trajectory, serving as the ground truth, while the irregular blue curve represents the measured trajectory by our EchoSpot.}
		\label{Rectangle}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/points.pdf}

		\caption{Locating a static person at different reference points, where the red points indicate the reference points and the blue points represent the spotted location by our EchoSpot.}
		\label{points}
	\end{minipage}

\end{figure*}

We place the speaker to be $100cm$ away from the wall and $110cm$ height above the ground in a room. 

\noindent{\bf \em Locating a static person.} We consider the distance ranges of $1m, 2m, 3m, 4m$ and $5m$ away from the speaker, and at each distance range, we take 10 different reference points. 
We ask a person to stand at the 10 different reference points corresponding to each distance range and use EchoSpot to spot his position.
At each reference point, this person will stand $10s$, so we can collect 50 location data at this point, giving that EchoSpot sends one chirp within each $200ms$.
We calculate these 50 positions and average them to be used as the located position of this person. 
Figure~\ref{Distance} shows the averaged localization error over the 10 reference points at each distance.
Specifically, EchoSpot achieves the averaged localization errors of $4.1cm,9.2cm,13.1cm,17.9cm,22.2cm$, respectively, at the distance of $1m, 2m, 3m, 4m$ and $5m$.
The median error is $12.4cm$, which is comparable to the state-of-the-art Wi-Fi and mmWave-based localization systems \cite{kotaru15:SIGCOMM:spotfi,xiong2013arraytrack} with median error over $23cm$.
We observe that the localization error grows with the increase of distance range.
This is due to the signal attenuation: the longer range will result in weak peak, making the peak selection more difficult. 





\noindent{\bf \em Locating a moving person.} We continue to show the performance of EchoSpot for localizing a moving person. 
We ask this person to walk in his natural speed and pattern along a predefined rectangle trajectory, indicated by the red line in Figure~\ref{Rectangle}.
The microphone and speaker are placed at the coordinate of $(0,0)$ and the wall is at $x=-0.5$.
The irregular blue curve represents the measured trajectory from EchoSpot by spotting a series of positions of the walking person. 
In this measured trajectory, the  averaged trajectory error is $21.9cm$.
As shown in the figure, when the person is far away from the wall, the trajectory error will become large.
The reason is that the wall reflection would be weaker than that from other objects, which may result in wrong peak selection corresponding to the wall reflection. 
However, we also observe when the person walks along the wall, the trajectory error is also relatively large.
The reason is that when a person walks along the wall, the error is caused by the arm or leg swing, resulting in the strongest peak appearing at the wrong place, leading to wrong peak selection and affecting the distance estimation.







We further compare its performance to the case that the static person stands on a series of reference points on the rectangle trajectory.
In Figure~\ref{points}, the red points indicate the reference points on the rectangle trajectory, and the blue points represent the spotted locations from the EchoSpot. 
Figure~\ref{CDF} shows the CDF of the trajectory error and the localization error, corresponding to Figure~\ref{Rectangle} and Figure~\ref{points}, respectively. 
From this figure, we can see the overall performance of EchoSpot for tracking a moving person is promising, even though it performs a little worse than that in the static status.
Specifically, the median errors are  $19.8cm$ and $8.5cm$, respectively, in the walking and static status. 
This is due to the fact that, when walking,  localization data collected by EchoSpot at each point is much less than that at the static status.
Thus, the calculation error from one location data will significantly impact the performance. 
However, in the static status, EchoSpot can collect 50 location data and average them, mitigating the errors from some location data. 
On the other hand, the movement of the leg and the arm will cause strong reflection, which will lead to the wrong peak selection for wall reflection. 
 



 \subsection{Impact of  Device Placement}
 \label{placement}
 

 \begin{figure*}%[!htb]
 	\centering
 	\begin{minipage}{0.30\textwidth}
 		\centering
 		\includegraphics[width=2.0in]{Fig/CDF.pdf}
 		\caption{CDF for Trajectory Error and Localization Error.}
 		\label{CDF}
 	\end{minipage}
 	\hspace{1em}
 	\begin{minipage}{0.30\textwidth}
 		\centering
 		\includegraphics[width=2.0in]{Fig/Seperation_err.pdf}
 		\caption{Impact of the speaker separation.}
 		\label{sepration}
 	\end{minipage}
 	\hspace{1em}
 	\begin{minipage}{0.30\textwidth}
 		\centering
 		\includegraphics[width=2.0in]{Fig/Height.pdf}
 		\caption{Impact of the device heights.}
 		\label{height}
 	\end{minipage}
 \end{figure*}
 
 We next conduct experiments to show the impact of device placement on the performance of EchoSpot.
 In a home environment, the devices may be placed differently, such as on the floor, on the coffee table or on a desk,  with different distances to the wall and having different heights. 
 We will vary the separation distance between the wall and the speaker, and the placement height to show EchoSpot's localization errors.

 
 

 First, we place the speaker at a $100cm$ desk and examine the impact of the distance between wall and speaker.
 We let a person  stand on four reference points on the straight line with $1m, 2m, 3m,$ and  $4m$ away from  the speaker.
At each point, the person will stay for $10s$, so that EchoSpot will get 50 position data and average them. 
 Figure \ref{sepration} plots the localization errors with different separation distance between the speaker and the wall.
 From this figure, we can see the  localization errors decrease first and then grow up at each distance range (i.e., $1m, 2m, 3m,$ and  $4m$).
 The lowest localization errors are all achieved at the separation distance of $100cm$.
 We notice that when the separation distance is small, the error is relatively high.
 The reason is that, according to Eqn.~(\ref{solution}),   a small distance (i.e., $w$) between wall and speaker will enlarge the distance estimation error of $D$, which thus will result in a large $x$.
On the other hand, we observe that the localization error will slightly increase when the separation distance is more than $100cm$. 
The reason is that when the separation distance increases, the signal path with respect to the wall reflection will become longer,  leading to more attenuation of the reflection signals. 
The peak value from the wall reflection will become smaller, making the peak selection incorrect. 
 
 

Next,  we use the same setting as the above experiment, except for fixing the distance between the wall and the speaker as $100cm$ and varying the height of the speaker from $0cm$ to $100cm$. 
Figure~\ref{height} shows the localization errors at the distance ranges of $1m, 2m, 3m, $ and $4m$ with respect to different heights of the speaker. 
From this figure, we can see the localization errors drop with the increase of height.
Specifically, when placing the speaker on the ground,  the shortest reflection is from the leg or feet.
From Section~\ref{sec:localization}, we know the duration of the virtual signal is determined by the height of person or height of the device.
When the device's height is larger than the half of the person's height, the duration is determined by the height of device and could be calculated through the device's height and shortest path.
Otherwise, we could not accurately measure the duration, leading to the wrong selection of wall reflection. 


\subsection{Impact of Moving Speed}
\label{sec_speed}


For the moving person setting, we further examine the impact of the walking speed to the  localization accuracy of EchoSpot.
We consider a person walking at a very low speed, at his normal speed, and at a high speed, respectively, toward the speaker from $4m$ to $1m$.
Figure~\ref{speed} shows the quartiles figure under three walking speeds.
The green point is the mean error and the orange horizontal line is the median error.
The maximum point represents 90th percentile error, and the minimum point represents the minimum error.
The upper  quartile represents 75th percentile error, and the lower  quartile represents 25th percentile error.
From this figure, we can see EchoSpot has the averaged error of $13.1cm$ at a very low speed, which increases to $19.2cm$ and $26.6cm$ at the normal and high speeds, respectively. 
The median errors are $13.8cm,18.5cm, 25.7cm$ and the 75th percentile errors are $17.1cm,23.2cm, 32.8cm$,  corresponding to the three walking speeds.
Obviously, the error of EchoSpot increases with the high walking speed, because when a person moves faster, the arm swing or leg swing will have more impact on the peak selection. 


\subsection{Performance at Different Room Layout}

 \begin{figure*}%[!htb]
	\centering
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/speed.pdf}
		\caption{Impact of the moving speed.}
		\label{speed}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/_room.pdf}
		\caption{Impact of different environments.}
		\label{room}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/_KKK.pdf}
		\caption{Impact of the Kalman filter.}
		\label{Kalman}
	\end{minipage}
\end{figure*}

We also deploy EchoSpot in three rooms with different layouts to show its performance. 
In all three rooms, the device is placed at $110cm$ height and at $100cm$ away from the wall. 
A person moves from $4m$ to $1m$ toward the speaker. 
Figure~\ref{room} shows the localization errors at each room.
At three rooms, the mean errors  are  $15.5cm, 14.8cm, 18.9cm$, the median errors  are  $15.5cm, 13.8cm, 15.7cm$, 
and the 75th percentile errors are $21.5cm, 21.9cm, 23.2cm$, respectively, without significant change.
This set of experiments demonstrate the robustness of EchoSpot in different layout environment. 



\subsection{Impact of Different Devices}


We evaluate the Echospot on various speakers, represented by Speaker 1 (Edifier R1280DB), Speaker 2 (Logitech z200), and Speaker 3 (Amazon Echo), to show that Echospot does not rely on specific hardware.
We tune the volume of these speakers so that they have similar transmission power. 
The Amazon Echo is connected via Bluetooth pairing, which may occur a certain latency. 
But notably, our Starting Error Cancellation module in Section~\ref{sec:sys_des} could eliminate such a latency.
For each speaker, we use the same frequency, i.e., $18-23khz$. 
We draw the CDFs of each device as shown in Figure \ref{device}.
The median errors for speaker 1, speaker 2, and speaker 3 are $16.5cm,17.9cm, $ and $20.4cm$, respectively.
We could observe a similar trend on these CDFs.
Notably, our system on Amazon Echo achieves slightly worse performance than that on Edifier R1280DB and Logitech z200.
The reason is that Amazon Echo generates the ultrasound to all directions, so the received signals will mix the reflection signals from different directions. 
This results in a relatively low SNR for the target signals, deteriorating the localization accuracy.
Also, we could see the maximum tracking error is about 40cm.
This is close to the width of the human body, so such an error is acceptable when tracking a person.
The similar CDFs on different devices implies that our system is robust against device heterogeneity.



\subsection{Impact of Noise}

We continue to conduct experiments to evaluate the  performance of Echospot under different types of noise: human talking and playing music.
The source of  noise is 0.5m away from the system.
Figure~\ref{noise} shows the errors under the silent, human talking and playing music environments.
From the figure, we observe EchoSpot performs similarly in the three environments, having the 75th percentile errors  of $21.5cm, 23.4cm, 22.6cm$, respectively.
The mean errors are $18.1cm, 19.5cm, 18.9cm$, respectively.
The median errors are  $18.5cm, 19.2cm, 18.7cm$, respectively.
Thus, we can conclude that these audible noises can only slightly affect EchoSpot.
The reason is that EchoSpot's working frequency is higher than the noise level.
To validate this point, we generate the spectrum under the scenarios of human talking and playing music when no human exists.
Figures~\ref{talk} and \ref{music} show the respective spectrum, which clearly show that all generated noises are much lower than 20khz.


We continue to consider more real scenarios when  playing the rock music and washing the utensils in the apartment.
Figures~\ref{rock} and \ref{wash}  show their respective spectrum. 
From the two figures, we could see sounds generated from these noise sources are still much lower than our working frequency, which shall have no overlapping with our Doppler shift frequency and can be easily subtracted via EchoSpot.
This further demonstrates that our EchoSpot is robust to the environmental noises.


\subsection{Impact of Location Correction Module}
We conduct experiments to validate the importance of {\em Location Correction (Kalman filter)} module in our system by comparing EchoSpot's performance with and without this module.
We ask a person to walk at his normal speeds towards the speaker from $4m$ to $1m$.
Figure~\ref{Kalman} compares the localization errors of EchoSpot  without  and with the Kalman filter.
From this figure, we can observe the 75th percentile errors of the two cases are $27.3cm$ and $ 21.5cm$, respectively.
The mean errors are $22.9cm$ and $18.1cm$, respectively.
The median errors are  $21.6cm$ and $ 18.4cm$, respectively.
Such  performance results indicate that  this module is important to  improve our system performance.

\begin{figure*}%[!htb]
	\centering
	\begin{minipage}{0.2\textwidth}
		\centering
		\includegraphics[width=1.4in]{Fig/talk.pdf}
		\caption{Spectrum in the human talking environment.}
		\label{talk}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.2\textwidth}
		\centering
		\includegraphics[width=1.4in]{Fig/music.pdf}
		\caption{Spectrum in the environment playing normal music.}
		\label{music}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.2\textwidth}
		\centering
		\includegraphics[width=1.4in]{Fig/rock.pdf}
		\caption{Spectrum in the environment playing rock music.}
		\label{rock}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.2\textwidth}
		\centering
		\includegraphics[width=1.4in]{Fig/wash.pdf}
		\caption{Spectrum in the environment washing the utensils.}
		\label{wash}
	\end{minipage}
\end{figure*}



\label{subsec:discussion}
\begin{figure*}%[!htb]
	\centering
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/CDF_D.pdf}
		\caption{Impact of the device.}
		\label{device}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/_noise.pdf}
		\caption{Impact of the noise.}
		\label{noise}
	\end{minipage}
\end{figure*}

















