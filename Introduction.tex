\section{INTRODUCTION}

Indoor localization has attracted wide research attention due to its potential of facilitating a variety of applications in smart homes such as security surveillance, elderly care, crowd monitoring, fitness tracking, etc.
A report has shown that a person may spend almost $88.9\%$ of the day indoors \cite{matz2014effects}. 
Also, the market value of indoor positioning and indoor navigation is expected to exceed  $\$23.6$ billion dollars in 2023~\cite{predict_indoor}, substantiating that there is a large demand for effective indoor localization technology.
The commonly used localization systems based on Global Positioning System (GPS) are not applicable to indoor environment, due to significant signal attenuation when penetrating the wall that leads to meter-level localization error.
This is unacceptably large in indoor environment, and reducing localization error at decimeter or centimeter level is highly desirable.

In the past decade, diverse technologies have been developed for indoor localization and tracking.
While extensive methods have been developed for localization via  camera~\cite{zhang2016litell,van2014camera}, motion sensor~\cite{langlois2017indoor}, inertial measurement unit (IMU)~\cite{yuan2014localization}, floor sensor~\cite{mirshekari2018occupant}, or light sensor~\cite{hu2018lightitude, zhu2017enabling}, they  either require a user to carry/wear sensors, or require to purchase and deploy the dedicate devices/sensors.
These methods have the drawbacks of inconvenience for use or causing privacy issues.
For example, wearable solutions may need the user  to wear the device all day for continuous monitoring.
The elders, sometimes, are likely to forget to wear devices, making it unsuitable for elderly monitoring.
Also, the wearable solutions are not suitable for some scenarios, such as localizing undefined people.
The camera-based solution could be accurate but likely to invade the user's privacy and make users feel uncomfortable.
The RFID localization \cite{ni2003landmarc,jin2006indoor,wang2013dude,wang2013rf}, has become popular recently due to its low cost and ease of use.
However, most RFID-based localization techniques are based on the assumption of knowing the tags' coordinates, which is impractical.
Besides, many RFID-based systems heavily rely on ideal propagation models of RF phase or the received signal strength indicator (RSSI), which may not be feasible.



On the other hand, the radio-frequency (RF) sensing solutions leveraging the Wi-Fi and mmWave for localization have been extensively explored, producing promising results, i.e., decimeter-level accuracy in the several meters sensing range. 
However, the Wi-Fi-based solutions~\cite{adib15:NSDI:multi, gjengset14:MobiCom:phaser, xiong2013arraytrack, vasisht16:NSDI:decimeter, xiong15:MobiCom:tonetrack, qian2018:mobisys:widar2, joshi:NSDI:2015wideo, soltanaghaei18:MobiSys:multipath, kotaru15:SIGCOMM:spotfi,  wang16:mobicom:lifs} occupy the data communication channels of 2.4GHz or 5GHz which are already crowded with data traffic, inevitably impacting the nearby devices to some extent, especially for those using multiple channels so as to achieve good performance. 
In addition, most of such systems require regular maintenance, or some of them need specialized signals, hindering their wide deployment.
The mmWave-based solutions~\cite{vasisht18:IMWUT:duet,zhao2019mid,gu19:MNSS:mmsense,pegoraro20::multi} do not cause interference to home devices, but they require the specialized mmWave radar which is typically expensive. 

In contrast to the aforementioned solutions, acoustic sensing is promising for localization which can take advantage of the ubiquitously available audio devices without competing for radio resources with other home devices.
The low sampling frequency of the audio signal enables signal processing to be implemented on a smart device.
Existing efforts~\cite{lin19:MobiCom:rebooting, cheng20:INFOCOM:acouradar, mao16:MobiCom:cat,nandakumar17:IMWUT:covertband, peng2012beepbeep, zhou17:ENS:battracker,zhang2018vernier, liu2015guoguo} for human localization via acoustic sensing are device-dependent, requiring users to carry smartphones. 
Although device-free (without carrying the devices) acoustic solutions \cite{gupta12:HFCS:soundwave, nandakumar2016fingerio, mao19:MobiCom:rnn, yun17:MobiSys:strata, chen2017echotrack, wang2016device, wang20:push} have been proposed for tracking the movement of hand, finger, or mouth, for the purposes of localization, activity recognition, or authentication, their effective sensing ranges are extremely limited, no more than 1 meter. 



In this paper, we propose a novel device-free localization solution via acoustic sensing, named EchoSpot, that can be implemented in the commercially available off-the-shelf (COTS) audio devices to work in home environments. 
Different from the existing work, we only rely on one speaker and one microphone with the reliance of wall reflection to precisely spot a human location in the two-dimensional space.
Specifically,  we program the audio device to control its built-in speaker to periodically emit inaudible FMCW (Frequency Modulated Continuous Wave) signals at 18kHz$\sim$23kHz and use the co-located microphone to receive the reflected signals from objects for analysis.
Based on the reflected signals, we generate time-of-flight (ToF) profile and aim to identify the peaks corresponding to the body reflection. 
By eliminating the influence from the device imperfection and from the environmental objects' reflection, we can identify ToF corresponding to the person reflection, allowing us to estimate the distance between the human body and the device.
To locate a person's position, we continue to develop a new solution to identify the ToF corresponding to the wall reflection and obtain the path of human-wall-device. 
Then, we can calculate the position information of a person in a two-dimensional space. 
Considering the potential impact of the multipath effect, we further apply the Kalman filter to correct the position information. 
In the end, EchoSpot is implemented on the commercial speaker and microphone, and deployed in the real home environments for performance evaluation.

Comparing to the existing approaches, our contributions can be summarized as follows.
\begin{itemize}
	
	\item We design a novel device-free localization system EchoSpot, by leveraging only one speaker and  one microphone for precisely locating a human. 
	It is a software-based system and can be implemented on the  COTS audio devices for conducting the localization services, rendering a wider application range in the general house environment.
	While leveraging the acoustic signals at $18kHz\sim23kHz$, it does not cause interference to the home Wi-Fi devices and is inaudible to human.
	
	
	\item A collection of acoustic signal processing techniques are developed, including generating the ToF profile, removing the impact from the device imperfection, environment objects, and multipath effect.
	In addition, a new solution is also proposed to identify the reflection path from the wall. 
	After applying these techniques,  EchoSpot can work suitably to spot a person's location in the home environment.
	
	\item We implement EchoSpot with the COTS speaker and microphone for proof-of-concept validation.
	We conduct experiments to demonstrate that EchoSpot can work effectively  both  to locate the static person and to track the moving person. 
	Experimental results exhibit that EchoSpot can achieve the median errors of  $8.5cm$ and $19.8cm$ for locating the static person and the moving person, respectively,  comparable to the reported results from the state-of-the-art.
	In addition, EchoSpot achieves the mean errors of $4.1cm, 9.2cm, 13.1cm, 17.9cm, $, $22.2cm$, respectively, at the distances of $1m, 2m, 3m, 4m$, and $5m$, for locating the static person.
\end{itemize}













